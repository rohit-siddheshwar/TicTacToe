package com.amazingapps.rgs.tic_tac_toe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


public class WinnerWho extends ActionBarActivity {

    TextView result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner_who);
        result = (TextView) findViewById(R.id.result);
        if(getIntent().getIntExtra("playerwon",0) == 1)
            result.setText("Player 1 Won The Game.");
        else
            result.setText("Player 2 Won The Game");
    }
    public void playAgain(View view) {
        Intent i = new Intent(this , Splash.class);
        startActivity(i);
    }
}
