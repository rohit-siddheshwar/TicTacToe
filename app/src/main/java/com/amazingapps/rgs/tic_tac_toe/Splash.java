package com.amazingapps.rgs.tic_tac_toe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class Splash extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    public void selectPlayer(View view) {
        Intent i = new Intent(this,SelectPlayerActivity.class);
        startActivity(i);
    }
}
