package com.amazingapps.rgs.tic_tac_toe;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;


public class SelectPlayerActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_player);
    }

    public void onePlayerSelected(View view) {
        Intent i = new Intent(this , OnePlayerGame.class);
        startActivity(i);
    }

    public void twoPlayerSelected(View view) {
        Intent i = new Intent(this , TwoPlayerGame.class);
        startActivity(i);
    }
}