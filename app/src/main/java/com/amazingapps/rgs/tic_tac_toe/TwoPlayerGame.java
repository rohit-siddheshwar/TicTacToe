package com.amazingapps.rgs.tic_tac_toe;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class TwoPlayerGame extends ActionBarActivity {

    String player1Sign,player2Sign;
    TextView title;
    Button b11,b12,b13,b21,b22,b23,b31,b32,b33;
    int flag = 0;
    int[][] board = new int[3][3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two_player_game);
        b11 = (Button) findViewById(R.id.button11);
        b12 = (Button) findViewById(R.id.button12);
        b13 = (Button) findViewById(R.id.button13);
        b21 = (Button) findViewById(R.id.button21);
        b22 = (Button) findViewById(R.id.button22);
        b23 = (Button) findViewById(R.id.button23);
        b31 = (Button) findViewById(R.id.button31);
        b32 = (Button) findViewById(R.id.button32);
        b33 = (Button) findViewById(R.id.button33);

        for(int j=0;j<3;j++)
        {
            for(int k=0;k<3;k++)
            {
                board[j][k] = -1;
            }
        }

        title = (TextView) findViewById(R.id.title);
        title.setText("Player 1's turn.");
        final AlertDialog.Builder alBuilder = new AlertDialog.Builder(this);
        alBuilder.setMessage("Select PLAYER-1's Sign :");
        alBuilder.setPositiveButton("X",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                player1Sign = "X";
                player2Sign = "O";
                dialog.dismiss();
            }
        });
        alBuilder.setNegativeButton("O" , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                player1Sign = "O";
                player2Sign = "X";
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alBuilder.create();
        alertDialog.show();
    }

    public void clicked11(View view) {
        onMyButtonClick(1,1);
    }

    public void clicked12(View view) {
        onMyButtonClick(1,2);
    }

    public void clicked13(View view) {
        onMyButtonClick(1,3);
    }

    public void clicked21(View view) {
        onMyButtonClick(2,1);
    }

    public void clicked22(View view) {
        onMyButtonClick(2,2);
    }

    public void clicked23(View view) {
        onMyButtonClick(2,3);
    }

    public void clicked31(View view) {
        onMyButtonClick(3,1);
    }

    public void clicked32(View view) {
        onMyButtonClick(3,2);
    }

    public void clicked33(View view) {
        onMyButtonClick(3,3);
    }

    public void resetGame(View view) {
        reseting();
    }

    private void reseting() {
        for(int j=0;j<3;j++)
        {
            for(int k=0;k<3;k++)
            {
                board[j][k] = -1;
            }
        }
        flag = 0;
        b11.setText("");
        b12.setText("");
        b13.setText("");
        b21.setText("");
        b22.setText("");
        b23.setText("");
        b31.setText("");
        b32.setText("");
        b33.setText("");
    }

    public void newGame(View view) {
        Intent i = new Intent(this , SelectPlayerActivity.class);
        startActivity(i);
    }

    public void onMyButtonClick(int r,int c)
    {
        int sig = 0;
        if(flag % 2 == 0) {
            sig = 1;
            title.setText("Player 2's turn.");
        }
        else
        {
            sig = 0;
            title.setText("Player 1's turn.");
        }
        if(board[r-1][c-1] == -1) {
            board[r - 1][c - 1] = sig;
            markOnScreen(r, c, sig);
            flag++;

            if (checkIfWin(sig)) {
                Intent i = new Intent(this, WinnerWho.class);
                i.putExtra("playerwon",sig);
                startActivity(i);
            }
            else {
                if (flag >= 9) {
                    Toast.makeText(this, "THE MATCH WAS DRAWN...PLAY AGAIN..", Toast.LENGTH_LONG).show();
                    reseting();

                }
            }
        }
        else
        {
            Toast.makeText(this,"Place Allready Taken",Toast.LENGTH_LONG).show();
        }
    }

    public boolean checkIfWin(int what)
    {
        int flag = 0;
        int i;
        for(i=0;i<3;i++)
        {
            if(board[i][0] == what && board[i][1] == what && board[i][2] == what)
            {
                flag = 1;
            }

            if(board[0][i] == what && board[1][i] == what && board[2][i] == what)
            {
                flag = 1;
            }

            if(board[0][0] == what && board[1][1] == what && board[2][2] == what)
            {
                flag = 1;
            }

            if(board[0][2] == what && board[1][1] == what && board[2][0] == what)
            {
                flag = 1;
            }
        }

        return flag == 1;
    }

    public void markOnScreen(int r,int c,int who)
    {
        String sign;
        if(who == 1)
        {
            sign = player1Sign;
        }
        else
        {
            sign = player2Sign;
        }
        if(r==1)
        {
            if(c==1)
            {
                b11.setText(sign);
            }
            else if(c==2)
            {
                b12.setText(sign);
            }
            else
            {
                b13.setText(sign);
            }
        }
        else if(r==2)
        {
            if(c==1){
                b21.setText(sign);
            }
            else if(c==2)
            {
                b22.setText(sign);
            }
            else
            {
                b23.setText(sign);
            }
        }
        else
        {
            if(c==1){
                b31.setText(sign);
            }
            else if(c==2)
            {
                b32.setText(sign);
            }
            else
            {
                b33.setText(sign);
            }
        }
    }
}
